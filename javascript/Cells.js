class Cells {

    constructor(rows, cols) {
        this.rows = rows;
    	this.cols = cols;
    	this.createHtmlDots();
		this.cells = new Array(rows);
		this.border = true;
		this.emptyCell = new Cell("");
		for (var row = 0; row < rows; ++row) {
			this.cells[row] = new Array(cols);
			for (var col = 0; col < cols; ++col) {
				this.cells[row][col] = new Cell(row + "_" + col);
			}
		}
		this.render();
	};

	readFromArray(array) {
    	this.clear();
    	var maxRow = 0;
    	var maxCol = 0;
    	for (var i in array) {
    	    maxRow = Math.max(maxRow, array[i][0]);
    	    maxCol = Math.max(maxCol, array[i][1]);
    	}
    	var rowOffset = Math.round(this.rows / 2 - maxRow / 2);
    	var colOffset = Math.round(this.cols / 2 - maxCol / 2);
    	for (var i in array) {
    		var row = parseInt(array[i][0]) + rowOffset;
    		var col = parseInt(array[i][1]) + colOffset;
    		if(row >= 0 && row < this.rows && col >= 0 && col < this.cols) {
    		    this.get(row, col).activate();
    		}
    	}
    };

	get(row, col) {
	    if(this.border) {
	        row = row < 0 ? this.rows + row : row >= this.rows ? row - this.rows : row;
            col = col < 0 ? this.cols + col : col >= this.cols ? col - this.cols : col;
        } else if(row < 0 || row >= this.rows || col < 0 || col >= this.cols) {
            return this.emptyCell;
        }
		return this.cells[row][col];
	};

	clear() {
		for (var row in this.cells) {
        	for (var col in this.cells[row]) {
				this.cells[row][col].clear();
			}
		}
	};
	
	reset() {
		for (var row in this.cells) {
        	for (var col in this.cells[row]) {
				this.cells[row][col].reset();
			}
		}
	};

	propagate() {
		for (var row in this.cells) {
			for (var col in this.cells[row]) {
				this.cells[row][col].propagate();
			}
		}
	};
	
	rollback() {
		for (var row in this.cells) {
        	for (var col in this.cells[row]) {
				this.cells[row][col].rollback();
			}
		}
	};

	render() {
        for (var row in this.cells) {
        	for (var col in this.cells[row]) {
    		    this.cells[row][col].render();
    	    }
        }
    };

    doStep() {
        this.propagate();
    	for (var row in this.cells) {
        	for (var col in this.cells[row]) {
    			var lifeCells = this.getLivingNeighbors(row, col);
    			this.cells[row][col].update(lifeCells);
    		}
    	}
    };

    getLivingNeighbors(row, col) {
        var lifeCells = 0;
        for(var r = 0; r < 3; ++r) {
            for(var c = 0; c < 3; ++c) {
                if(r != 1 || c != 1) {
                    lifeCells += this.get(row - 1 + r, col -1 + c).isAlive() ? 1 : 0;
                }
            }
        }
    	return lifeCells;
    };

    createHtmlDots() {
    	var cellString = "";
    	for (var row = 0; row < this.rows; ++row) {
    		for (var col = 0; col < this.cols; ++col) {
    			cellString += "<div class='dot' id='" + row + "_" + col + "'></div>";
        	}
    	}
    	var cellParent = $("#cells");
    	cellParent.css("grid-template", "repeat(" + this.rows + ", 1fr) / repeat(" + this.rows + ", 1fr)");
    	cellParent.css("width", screen.height - 350);
    	cellParent.css("height", screen.height - 350);
    	cellParent.empty();
    	cellParent.append(cellString)
    };

}