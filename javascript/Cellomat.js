class Cellomat {

    constructor() {
	    this.cells = new Cells(60, 60);
	    this.speed = 1000;
	    this.step = 0;
	    this.running = false;
	    this.library = new Library(this);
	    this.reset();
	}

	setRunning(running) {
		this.running = running;
		if (this.running) {
			$("#startstopbutton").addClass("bi-play-fill");
			$("#startstopbutton").removeClass("bi-play");
			$("#startstopbutton").prop("title", "Stop");
		} else {
			$("#startstopbutton").removeClass("bi-play-fill");
			$("#startstopbutton").addClass("bi-play");
			$("#startstopbutton").prop("title", "Start");
		}
	};

	doOneStep() {
		this.cells.doStep();
		this.setStep(this.step + 1);
		$("#stepBack").prop("disabled",false);
		$("#reset").prop("disabled",false);
	};

	clear() {
		this.library.deselectAll();
		this.clearCells();
	};

	clearCells() {
	    this.cells.clear();
	    this.reset();
	}

	reset() {
		this.cells.reset();
		this.setStep(0);
		this.setRunning(false);
		$("#stepBack").prop("disabled",true);
		$("#reset").prop("disabled",true);
	};

	setStep(step) {
	    this.step = step;
		document.getElementById("stepcount").innerHTML = this.step;
	};

	readFromArray(array) {
	    this.cells.clear();
		this.cells.readFromArray(array);
	};

	start() {
		this.setRunning(!this.running);
		this.run();
	}

	run() {
		if (this.running) {
			this.doOneStep();
			var self = this;
			setTimeout(function() {
				self.run();
			}, 1000 - self.speed);
		}
	};

	stepBack() {
		this.setRunning(false);
		this.cells.rollback();
		this.setStep(this.step - 1);
		$("#stepBack").prop("disabled",true);
	};

	doStep() {
		this.setRunning(false);
		this.doOneStep();
	};

	setSize() {
	    this.reset();
	    var size = parseInt($("#size").val());
	    this.cells = new Cells(size, size);
	}

	setSpeed() {
	    this.speed = parseInt($("#speed").val());
	}

	changeBorder() {
	    this.cells.border = !this.cells.border;
	    if (this.cells.border) {
        	$("#borderbutton").addClass("bi-border-outer");
        	$("#borderbutton").removeClass("bi-border");
        	$("#borderbutton").prop("title", "Gitter begrenzen");
        	$("#cells").removeClass("cellsborder");
        } else {
        	$("#borderbutton").removeClass("bi-border-outer");
        	$("#borderbutton").addClass("bi-border");
        	$("#borderbutton").prop("title", "Gitter als umlaufender Torso");
        	$("#cells").addClass("cellsborder");
        }
	}

}