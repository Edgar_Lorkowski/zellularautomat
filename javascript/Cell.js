class Cell {

    constructor(identifier) {
        this.identifier = identifier;
	    this.startValue = false;
	    this.touched = false;
	    this.oldValue = false;
	    this.newValue = false;
	    if(identifier !== "") {
            this.addEventListeners();
            this.render();
        }
	}

	isAlive() {
	    return this.oldValue;
	}

    update(livingNeighbors) {
        // Game-Of-Live rules
    	switch (livingNeighbors) {
    		case 2: // stay
    			this.setNewValue(this.oldValue);
    			break;
    		case 3: // live
    			this.setNewValue(true);
    			break;
    		default: // die
    		    this.setNewValue(false);
    	}
    };

    setNewValue(newValue) {
        if(newValue) {
            this.touched = true;
        }
        this.newValue = newValue;
        this.render();
    }

    activate() {
		this.startValue = true;
		this.reset();
	};

	clear() {
		this.startValue = false;
		this.reset();
	};
	
	reset() {
	    this.touched = false;
		this.setNewValue(this.startValue);
		this.oldValue = false;
		this.render();
	};

	invert() {
		this.newValue = !this.newValue;
		this.startValue = this.newValue;
		this.touched = this.startValue;
		this.render();
	};

	propagate() {
	    if(this.newValue != this.oldValue) {
		    this.oldValue = this.newValue;
		}
	};
	
	rollback() {
		this.setNewValue(this.oldValue);
	};

	render() {
	    if(this.identifier !== "") {
            $("#" + this.identifier).css('background-color', this.newValue ? "black" : this.touched ? "lightgrey" : "#ececec");
        }
    };

    addEventListeners() {
        var self = this;
       	$("#" + this.identifier).mousedown(function(e) {
       		self.invert();
       	});
       	$("#" + this.identifier).mouseenter(function(e) {
       		if (e.buttons == 1) {
       			self.invert();
       		}
       	});
    }
}