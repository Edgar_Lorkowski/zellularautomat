class Library {

    constructor(cellomat) {
        this.cellomat = cellomat;
        fetch('lexicon.txt')
            .then(response => response.text())
            .then(data => {
                this.readLexicon(data);
            });
    }

    readLexicon(data) {
        this.templates = new Array();
        this.terms = new Array();
        var self = this;
        var lines = data.split('\n');
        for(var line = 0; line < lines.length; line++) {
            if(lines[line].startsWith(":")) {
                var index = lines[line].indexOf(":", 1);
                var name = lines[line].charAt(1).toUpperCase() + lines[line].substring(2,index);
                var text = lines[line].substr(index + 1).trim() + " ";
                while(!lines[++line].trim().startsWith(".") && !lines[line].trim().startsWith(":")
                    && !lines[line].trim().startsWith("*") && !lines[line].trim().startsWith("-----")) {
                    text += lines[line].trim() + " ";
                }
                text = text.replaceAll("{", "<a href='#' onclick='cellomat.library.showPopup(this)'>");
                text = text.replaceAll("}", "</a>");
                var template = new Template(name, text);
                if(lines[line].trim().startsWith(".") || lines[line].trim().startsWith("*")) {
                    template.addLine(lines[line].trim());
                    while(lines[++line].trim().startsWith(".") || lines[line].trim().startsWith("*")) {
                        template.addLine(lines[line].trim());
                    }
                    this.templates.push(template);
                } else {
                    this.terms.push(template);
                }
            }
        }
        var select = $("#library");
        for(var i in this.templates) {
            select.append("<option value=\"" + i + "\">" + this.templates[i].name + "</option>");
        }
        select.on('change click', function() {
            self.setCells(self.templates[this.value]);
        });
    }

    setCells(template) {
        this.cellomat.clearCells();
        var liveCells = [];
        for(var i in template.lines) {
            var charArray = template.lines[i].split('');
            for(var c in charArray) {
                if(charArray[c] == '*') {
                    liveCells.push([i, c]);
                }
            }
        }
        this.setTemplateNameAndText(template);
        this.cellomat.cells.readFromArray(liveCells);
        this.cellomat.start();
    }

    deselectAll() {
        $('#library option').each(function() {
             $(this).prop('selected', false);
        });
        this.clearTemplate();
    }

    clearTemplate() {
        $("#templateName").empty();
        $("#templatetext").empty();
    }

    setTemplateNameAndText(template) {
        this.clearTemplate();
        $("#templateName").append(template.name);
        $("#templatetext").append(template.text);
    }

    showPopup(x) {
        var name = x.innerHTML.toLowerCase();
        for(var term in this.terms) {
            if(this.terms[term].name.toLowerCase() === name) {
                this.setTemplateNameAndText(this.terms[term]);
                return;
            }
        }
        for(var template in this.templates) {
            if(this.templates[template].name.toLowerCase() === name) {
                this.setCells(this.templates[template]);
                $("#library").val(template);
                return;
            }
        }
    }

}




class Template {

    constructor(name, text) {
        this.name = name;
        this.text = text;
        this.lines = new Array();
    }

    addLine(line) {
        this.lines.push(line);
    }

}
